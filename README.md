Iceye Map View Test
================

Getting started:
----------------
In order to run Cesium applications, we’ll need a local web server to host our files. We’ll be using Node.js for this example. If you already have a web server that you want to use, that’s fine too. Cesium has no server requirements; it is completely client side. This means any web server that can host static content can also host Cesium.

Setting up a web server with Node.js is easy and only takes 3 steps: 

1. Install Node.js from their website (https://nodejs.org), you can use the default install settings. 
2. Open a command shell in the Cesium root directory and download/install the required modules by executing npm install. This will create a ‘node_modules’ directory in the root directory.
3. Finally, start your web server by executing node server.js in the root directory. 
4. Open the server in the URL http://localhost:8080
